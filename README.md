Use ./screenshots.sh in a directory where you have a Pebble app project, to generate screenshots of the app on the various platforms (aplite, basalt, chalk).

E.g.

./screenshots --outdir=screenshots --nobasalt S 3 B D U

will simulate your app on all platforms except basalt, save screenshots under a directory called screenshot (will be created if it does not exist yet) and perform the following action:
1. Simulate pressing the select button on the Pebble
2. Wait 3 seconds
3. Simulate pressing the back button on the Pebble
4. Simulate pressing the down button on the Pebble
5. Simulate pressing the up button on the Pebble

After each step above a new screenshot is saved. The files are named $platform/$N.png, where $platform is the current platform (aplite, basalt, chalt) and $N is the number of the step.

[Bitcoin](bitcoin:1GH8eMZ4bc1qxB5R8DQkhCYZAU2ttD9Pka)

![bitcoin:1GH8eMZ4bc1qxB5R8DQkhCYZAU2ttD9Pka](https://i.imgur.com/hBbicxG.png)

