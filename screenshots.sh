#!/bin/bash
# Small script to run the Pebble emulator on different platforms, but using the same sequenece of button presses
# After each command a new screenshot is take, and saved into the given directory
# Requires xdotool (e.g. Debian/Ubuntu: apt-get install xdotool, Fedora: yum install xdotool)

function frame {
	if [[ $have_convert == 1 ]]; then
		platform=$1
		image=$2
		if [[ $platform == "aplite" || $platform == "basalt" ]]; then
			convert $outfile -bordercolor black -border 1x1 $outfile
		elif [[ $platform == "chalk" ]]; then
			convert $outfile -gravity center +antialias -fill none -background none -extent 182x182 -stroke black -draw "circle 90.5,90.5 181,90.5" $outfile
		fi
	fi
}

platforms=("aplite" "basalt" "chalk")

which convert >/dev/null
have_convert=0
if [[ $? == "0" ]]; then
	have_convert=1
fi

# Check if the user has xdotool installed
which xdotool >/dev/null
if [[ $? != 0 ]]; then
	echo "You need to install xdotool to use this script." 1>&2
	which apt-get >/dev/null
	if [[ $? == 0 ]]; then
		echo "Try with: apt-get install xdotool" 1>&2
	else
		which dnf >/dev/null
		if [[ $? == 0 ]]; then
			echo "Try with: dnf install xdotool" 1>&2
		else
			which yum >/dev/null
			if [[ $? == 0 ]]; then
				echo "Try with: yum install xdotool" 1>&2
			else
				echo "Try searching for xdotool among the software your distribution provides, or go to http://www.semicomplete.com/projects/xdotool/"
			fi
		fi
	fi
	exit
fi

outdir=$PWD
htmlout=0
nosubdirs=0

# Parse command line arguments
usage="`basename $0` [--help] [--outdir=DIR] [--htmlout] [--nosubdirs] [--noframe] [--noaplite] [--nobasalt] [--nochalk] [command] [command] ..\noutdir set the directory where screenshots are saved. It will be created if it does not exist.\nThe htmlout option prints a table containing the screenshots in HTML format to stdout.\nThe nosubdirs option places all saved files in the same directory.\nThe noframe options disables drawing a frame around screenshots.\nThe other no* options disable building on those platforms.\ncommand is one of B (back), U (up), D (down) or S (select) or an integer which specifies how long to wait (in seconds) before moving to the next step."
for arg in $@; do
	case "$arg" in
		--help) echo -e $usage 1>&2; exit
			;;
		--outdir=*) outdir="${arg#*=}"
			;;
		--htmlout) htmlout=1
			;;
		--nosubdirs) nosubdirs=1;
			;;
		--noframe) have_convert=0;
			;;
		--noaplite) platforms[0]=""
			;;
		--nobasalt) platforms[1]=""
			;;
		--nochalk) platforms[2]=""
			;;
		*) break
			;;
	esac
	shift
done

if [[ nosubdirs == 0 ]]; then
	file_path_format="$outdir/%s/%d.png"
else
 	file_path_format="$outdir/%s_%d.png"
fi

tmp=()
for p in "${platforms[@]}"; do
	[[ "$p" != "" ]] && tmp+=($p)
done
platforms=("${tmp[@]}")

commands=$@

step=0

for platform in "${platforms[@]}"; do
	step=0
	mkdir -p $outdir
	[[ $nosubdirs == 0 ]] && mkdir -p $outdir/$platform
	pebble install --emulator $platform 1>&2
	outfile=`printf $file_path_format $platform $step`
	pebble screenshot $outfile 1>&2
	frame $platform $outfile

	WID=`xdotool search QEMU | head -1`
	for cmd in $commands; do
		case $cmd in
			"B") key="Left"
				;;
			"U") key="Up"
				;;
			"D") key="Down"
				;;
			"S") key="Right"
				;;
			[0-9]*) echo "Sleeping for $cmd"; sleep $cmd; key="" 1>&2
				;;
			*) echo "Invalid command $cmd. Should be one of B, U, D, S or an integer" 1>&2; exit
				;;
		esac
		if [[ $key != "" ]]; then
			xdotool key $key 1>&2
			sleep 1
		fi
		let step=step+1
		outfile=`printf $file_path_format $platform $step`
		pebble screenshot $outfile 1>&2
		frame $platform $outfile
	done
		
	pebble kill 1>&2
done

if [[ $htmlout == 1 ]]; then
	echo "<html><head><title>Screenshots</title></head><body><table>"
	echo "<tr>"
	for platform in "${platforms[@]}"; do
		echo "<th>$platform</th>"
	done
	echo "</tr>"
	for i in `seq 0 $step`; do
		echo "<tr>"
		for platform in "${platforms[@]}"; do
			outfile=`printf $file_path_format $platform $i`
			echo -n "<td style='border: 1px solid black; background-color:grey;'><img src='$outfile'/></td>"
		done
		echo "</tr>"
	done
	echo "</table></body></html>"
fi
